from .event import Event2, Event3


class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat.failed")
        #self.add_prop("eated-")
        self.inform("eat")
