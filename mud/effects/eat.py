from .effect import Effect2, Effect3
from mud.events import EatEvent

class EatEffect(Effect2):
    EVENT = EatEvent
